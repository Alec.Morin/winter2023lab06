public class Jackpot{
	
	public static void main(String[]args){
		
		System.out.println("Welcome to Jackpot!");
		Board play = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while (!(gameOver)){
			System.out.println(play);
			boolean turn = play.playATurn();
			if (turn){
				gameOver = true;
			}
			else{
				numOfTilesClosed++;
			}
		}
		
		if (numOfTilesClosed >= 7){
			System.out.println("You have reached the jackpot, You win!!");
		}
		else{
			System.out.println("You lost the jackpot");
		}
	}
}