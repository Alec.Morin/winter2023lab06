import java.util.Random;
public class Die{
	private int faceValue;
	private Random randNum;
	
	public Die(){
		this.faceValue = 1;
		this.randNum = new Random();
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
		
	public int roll(){
		return this.faceValue = randNum.nextInt(1,6);
	}
	
	public String toString(){
		return "faceValue: " + this.faceValue;
	}
}