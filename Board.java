public class Board{
	private Die Die1;
	private Die Die2;
	private boolean[] tiles;
	
	
	public Board(){
		this.Die1 = new Die();
		this.Die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public boolean playATurn(){
		this.Die1.roll();
		this.Die2.roll();
		System.out.println(this.Die1 + "\n" + this.Die2);
		int sumOfDice = this.Die1.getFaceValue() + 	this.Die2.getFaceValue();
		
		if (!(this.tiles[sumOfDice -1])){
			this.tiles[sumOfDice -1] = true;
			System.out.println("Closing tile equal to sum: " + this.tiles[sumOfDice -1]);
			return false;
		}
		else if (this.tiles[sumOfDice -1] && !(this.tiles[this.Die1.getFaceValue()])){
			this.tiles[this.Die1.getFaceValue()] = true;
			System.out.println("Closing tile with the same value as die one: " + this.tiles[this.Die1.getFaceValue()]);
			return false;
		}
		else if (this.tiles[sumOfDice -1] && !(this.tiles[this.Die2.getFaceValue()])){
			this.tiles[this.Die2.getFaceValue()] = true;
			System.out.println("Closing tile with the same value as die one: " + this.tiles[this.Die2.getFaceValue()]);
			return false;
		}
		else{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
	
	public String toString(){
		String declaration = "";
		for (int i = 0; i<this.tiles.length; i++){
			if (!(this.tiles[i])){
				declaration = declaration + (i+1) + " ";
			}
			else{
				declaration = declaration + "X ";
			}
		}
		return declaration;
	}
}